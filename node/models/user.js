const mongoose = require("mongoose");
const { v4: uuidv4 } = require("uuid");
const crypto = require("crypto");
const { ObjectId } = mongoose.Schema;

const opts = { toJSON: { virtuals: true } };

const userSchema = new mongoose.Schema(
  /// -------- login -------- ///
  {
    name: {
      type: String,
      trim: true,
    },
    email: {
      type: String,
      trim: true,
      // required: true,
    },

    /// -------- profile -------- ///

    // -------- AUTH -------- ///
    role: {
      type: String,
      enum: ["user", "admin"],
      default: "user",
    },
    hashed_password: {
      type: String,
    },
    salt: String,
    created: {
      type: Date,
      default: Date.now,
    },
    updated: Date,

    // -------- END -------- ///
  },
  opts
);

/**
 * Virtual fields are additional fields for a given model.
 * Their values can be set manually or automatically with defined functionality.
 * Keep in mind: virtual properties (password) don’t get persisted in the database.
 * They only exist logically and are not written to the document’s collection.
 */

// virtual field
userSchema
  .virtual("password")
  .set(function (password) {
    // create temporary variable called _password
    this._password = password;
    // generate a timestamp
    this.salt = uuidv4();
    // encryptPassword()
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function () {
    return this._password;
  });

// methods
userSchema.methods = {
  authenticate: function (plainText) {
    return this.encryptPassword(plainText) === this.hashed_password;
  },

  encryptPassword: function (password) {
    if (!password) return "Please enter a password";
    try {
      const hash = crypto
        .createHmac("sha1", this.salt)
        .update(password)
        .digest("hex");

      return hash;
    } catch (err) {
      return err;
    }
  },
};

module.exports = mongoose.model("User", userSchema);
