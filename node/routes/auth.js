const express = require("express");
const { signIn, signUp, signOut, verify, protectedRoute } = require("../controllers/auth");

const router = express.Router();

router.post("/sign-in", signIn);
router.post("/sign-up", signUp);
router.post("/sign-out", protectedRoute, signOut);
router.post("/verify",protectedRoute, verify);

module.exports = router;
