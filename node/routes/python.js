const express = require("express");
const {bars} = require("../controllers/bars");
const {calculation} = require("../controllers/calculation");


const router = express.Router();

router.post("/bars", bars);
router.post("/calculation", calculation);


module.exports = router;

