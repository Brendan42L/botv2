require("dotenv").config();
const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const cookieParser = require("cookie-parser");
var morgan = require("morgan");
require("mongoose-function")(mongoose);
require("dotenv").config();

const { strategyOne, strategyTwo } = require("./strategies");


// datebase
mongoose
  .connect(process.env.MONGO_URI)
  .then(() => console.log("DataBase Connected"))
  .catch((err) => {
    console.log(`DataBase connection error: ${err.message}`);
  });

mongoose.connection.on("error", (err) => {
  console.log(`DataBase connection error: ${err.message}`);
});

//routes
// const userRoutes = require("./routes/user");
const authRoutes = require("./routes/auth");
const pythonRoutes = require("./routes/python");

var corsOptions = {
  origin: process.env.CLIENT_URL,
  credentials: true,
};

//middleware
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));
app.use("/", authRoutes);
app.use("/", pythonRoutes);

app.use(function (err, req, res, next) {
  if (err.name === "UnauthorizedError") {
    res
      .status(401)
      .json({ error: "Unauthorized, your not meant to be here.... go away!" });
  }
});

// listen on port
const port = process.env.PORT || 8080;
app.listen(port, () => {
  console.log(`Server Started on port ${port}`);
});



// const tradingBot = async () => {
//   const scanMarkets = async () => {
//     const start = moment().subtract(1, "days").format();
//     const end = moment().subtract(15, "minutes").format();
//     const bars = await getBars("BTCUSD", start, 15);
//     strategyOne(bars)
//       .then((data) => {
//         console.log("Strategy One Result", data);
//       })
//       .catch((err) => {
//         console.log("strategyOne - error", err);
//       });
//   };
//   scanMarkets();
//   setInterval(scanMarkets, 60 * 1000);
// };
// tradingBot();
