var CrossDownIndicator = require("technicalindicators").CrossDown;

module.exports = crossDown = async (data) => {
  var input = {
    lineA: [7, 6, 5, 4, 3, 8, 3, 5, 3, 8, 5, 5, 3, 8, 5, 5, 8, 3],
    lineB: [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
};

  const result = CrossDownIndicator.calculate(input);

  return result;
};
