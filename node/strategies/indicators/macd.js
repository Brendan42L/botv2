var MACD = require("technicalindicators").MACD;

module.exports = macdIndicator = (data) => {
    
  const closePrice = data.map((bar) => parseFloat(bar.ClosePrice.toFixed(2)));

  var macdInput = {
    values: closePrice,
    fastPeriod: 5,
    slowPeriod: 8,
    signalPeriod: 3,
    SimpleMAOscillator: false,
    SimpleMASignal: false,
  };

  const macd = MACD.calculate(macdInput);

  return macd;
};
