const SMA = require("technicalindicators").SMA;

module.exports = smaIndicator = (data) => {
  let period = 200;

  const closePrice = data.map((bar) => parseFloat(bar.ClosePrice.toFixed(2)));

  const sma = SMA.calculate({ period: period, values: closePrice }).map(
    (value) => parseFloat(value.toFixed(2))
  );
  return sma;
};
