var CrossUpIndicator = require("technicalindicators").CrossUp;

module.exports = crossUp = async (lineA, lineB) => {
  var input = {
    lineA: lineA,
    lineB: lineB,
  };

  const result = CrossUpIndicator.calculate(input);
  console.log(result);
  // return result;
};
