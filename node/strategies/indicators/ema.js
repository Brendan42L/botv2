const EMA = require("technicalindicators").EMA;

module.exports = emaIndicator =  (data) => {
  // calculate EMA
  let period = 50;

  const closePrice = data.map((bar) => parseFloat(bar.Close.toFixed(2)));

  const ema = EMA.calculate({ period: period, values: closePrice }).map(
    (value) => parseFloat(value.toFixed(2))
  );
  return ema;
};
