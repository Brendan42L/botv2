const strategyOne = require("./strategy-one");
const strategyTwo = require("./strategy-two");

module.exports = { strategyOne, strategyTwo };
