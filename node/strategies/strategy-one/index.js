const emaIndicator = require("../indicators/ema");
const smaIndicator = require("../indicators/sma");
const rsiIndicator = require("../indicators/rsi");
const macdIndicator = require("../indicators/macd");
const crossUp = require("../indicators/crossUp");
const crossDown = require("../indicators/crossDown");

const sendEmail = require("../../utils/mailer");

var http = require("http");

module.exports = strategyOne = async (data) => {
  const returnMailStructure = (data) => {
    const emailData = {
      from: "brendanlittle42@gmail.com",
      to: "brendanlittle42@gmail.com",
      subject: "Check Trade",
      text: `${data}`,
      html: ``,
    };
    return emailData;
  };

  const BARS = data;
  let signals;

  // // all values
  const RSI = rsiIndicator(data);
  const EMA = emaIndicator(data);
  const CLOSE_PRICES = BARS.map((bar) => {
    return bar.ClosePrice;
  });

  // latest
  const _RSI_LATEST = RSI[RSI.length - 1];

  const _EMA_LATEST = EMA[EMA.length - 1];
  const CLOSE_PRICE_LATEST = BARS[BARS.length - 1].Close;

  function HttpRequest(filename, callback) {
    const value = JSON.stringify({ bars: BARS, rsi: RSI });

    var options = {
      hostname: process.env.PYTHON_URL,
      path: "/divergence",
      port: 8000,
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Content-Length": value.length,
      },
    };

    var req = http.request(options, function (resp) {
      resp.on("data", callback);
    });

    req.on("error", function (e) {
      console.log("Request to '" + filename + "' failed: " + e.message);
    });

    req.write(value);
    req.end();
  }

  HttpRequest("data", async (data) => {
    signals = JSON.parse(JSON.parse(data));

    const regularBull = Object.values(signals.regularBull);
    const hiddenBull = Object.values(signals.hiddenBull);
    const regularBear = Object.values(signals.regularBear);
    const hiddenBear = Object.values(signals.hiddenBear);
    const _REGULAR_BULL_LATEST = regularBull[regularBull.length - 1];
    const _REGULAR_BEAR_LATEST = regularBear[regularBear.length - 1];
    console.log("http ran", regularBull,regularBear)
    if (_REGULAR_BULL_LATEST) {
      if (CLOSE_PRICE_LATEST < _EMA_LATEST) {
        console.log("Bull Div", "Price < EMA", "Place Trade Long");
        await sendEmail(
          returnMailStructure("Bull Div: Price < EMA: Place Trade Long")
        );
      } else {
        console.log("Bull Div Detected - No trade made");
        await sendEmail(
          returnMailStructure("Bull Div Detected: No trade made")
        );
      }
    }
    if (_REGULAR_BEAR_LATEST) {
      if (CLOSE_PRICE_LATEST > _EMA_LATEST) {
        console.log("Bear Div", "Price > EMA", "Place Trade Short");
        await sendEmail(
          returnMailStructure("Bear Div: Price > EMA: Place Trade Short")
        );
      } else {
        console.log("Bear Div Detected - No trade made");
        await sendEmail(
          returnMailStructure("Bear Div Detected: No trade made")
        );
      }
    }
  });

  return "";
};
