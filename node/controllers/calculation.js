const sendEmail = require("../utils/mailer");

exports.calculation = async (req, res) => {
  const data = JSON.parse(req.body.signals);

  const returnMailStructure = (data) => {
    const emailData = {
      from: "brendanlittle42@gmail.com",
      to: "brendanlittle42@gmail.com",
      subjxect: "Check Trade",
      text: `${data}`,
      html: ``,
    };
    return emailData;
  };

  let signals;

  signals = data;

  const regularBull = Object.values(signals.regularBull);
  const hiddenBull = Object.values(signals.hiddenBull);
  const regularBear = Object.values(signals.regularBear);
  const hiddenBear = Object.values(signals.hiddenBear);
  const _REGULAR_BULL_LATEST = regularBull[regularBull.length - 1];
  const _REGULAR_BEAR_LATEST = regularBear[regularBear.length - 1];

  if (_REGULAR_BULL_LATEST) {
    if (CLOSE_PRICE_LATEST < _EMA_LATEST) {
      console.log("Bull Div", "Price < EMA", "Place Trade Long");
      await sendEmail(
        returnMailStructure("Bull Div: Price < EMA: Place Trade Long")
      );
    } else {
      console.log("Bull Div Detected - No trade made");
      await sendEmail(returnMailStructure("Bull Div Detected: No trade made"));
    }
  }
  if (_REGULAR_BEAR_LATEST) {
    if (CLOSE_PRICE_LATEST > _EMA_LATEST) {
      console.log("Bear Div", "Price > EMA", "Place Trade Short");
      await sendEmail(
        returnMailStructure("Bear Div: Price > EMA: Place Trade Short")
      );
    } else {
      console.log("Bear Div Detected - No trade made");
      await sendEmail(returnMailStructure("Bear Div Detected: No trade made"));
    }
  }
  return res.status(200).json(data);
}
