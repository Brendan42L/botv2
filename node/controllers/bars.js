const getBars = require("./gateway/index");
const moment = require("moment");
const rsiIndicator = require("../strategies/indicators/rsi");

exports.bars = async (req, res) => {

  const start = moment().subtract(1, "days").format();
  const end = moment().subtract(15, "minutes").format();
  const bars = await getBars("BTCUSD", start, 15);
  const value = JSON.stringify({ bars: bars, rsi: rsiIndicator(bars) });
  return res.status(200).json(value);
  
}


