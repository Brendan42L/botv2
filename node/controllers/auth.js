const User = require("../models/user");
const JWT = require("jsonwebtoken");
var { expressjwt: jwt } = require("express-jwt");

exports.signIn = async (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email }, (err, user) => {
    // if err or no user
    if (err || !user) {
      return res.status(401).json("User with that email does not exist");
    }
    // if user is found make sure the email and password match
    // create authenticate method in model and use here
    if (!user.authenticate(password)) {
      return res.status(401).json("Email and password do not match");
    }
    // generate a token with user id and secret
    const token = JWT.sign({ _id: user._id }, process.env.JWT_SECRET);
    const { _id, name, email, role } = user;

    let remember = new Date();
    remember.setTime(remember.getTime() + 3600000 * 24 * 3);
    res.cookie("TOKEN", token, { expires: remember });

    return res.json({ token, user: { _id, email, name, role } });
  });
};

exports.signOut = async (req, res, next) => {
  try {
    var decoded = req.body.token
      ? JWT.verify(req.body.token, process.env.JWT_SECRET)
      : false;
    if (decoded) {
      res.clearCookie("TOKEN");
      res.status(200).json("goodbye");
      res.end();
    } else {
      res.status(401).json("invalid token");
    }
  } catch (err) {
    console.log("ERROR:", err);
  }
};

exports.verify = async (req, res, next) => {
  try {
    var decoded = req.body.token
      ? JWT.verify(req.body.token, process.env.JWT_SECRET)
      : false;
    if (decoded) {
      res.status(200).json("valid token");
    } else {
      res.status(401).json("invalid token");
    }
  } catch (err) {
    res.status(401).json("invalid token");
  }
};

// protect routes for signed in users using jsonWebToken
exports.protectedRoute = jwt({
  secret: process.env.JWT_SECRET,
  algorithms: ["HS256"],
  userProperty: "auth",
});

// to create admin - no client side methods allowed
exports.signUp = async (req, res, next) => {
  // const userExists = await User.findOne({ email: req.body.email });
  // if (userExists)
  //   return res.status(403).json({
  //     errs: "Email is taken!",
  //   });
  // const user = await new User(req.body);
  // await user.save();
  // res.status(200).json({ message: "sign up success! Please login." });
};
