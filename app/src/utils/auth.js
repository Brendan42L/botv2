import { useState } from "react";
import { mongoDBRequest } from "../services/api/dbRequest";
import { useNavigate } from "react-router-dom";

const AuthLogic = () => {
  const token = localStorage.getItem("token");
  const profile = localStorage.getItem("user");
  const [auth, setAuth] = useState(false);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [user, setUser] = useState(JSON.parse(profile));
  const navigate = useNavigate();


  const authenticate = () => {
    setIsLoading(true);
    mongoDBRequest("verify", "POST", { token: token }, token)
      .then((data) => {
        if (data === "valid token") {
          setAuth(true);
          setIsLoading(false);
          setInterval(() => {
            authenticate();
            navigate(0);
          }, 300000);
          setTimeout(() => {
            localStorage.removeItem("token");
            localStorage.removeItem("user");
          }, 1000 * 60 * 60 * 24 * 1);
        } else {
          setIsLoading(false);
          setAuth(false);
          setError("Could not authenticate");
        }
      })
      .catch((err) => {
        setIsLoading(false);
        setAuth(false);
        console.log("Errror", err);
        setError("Opps Something went wrong please check the console");
      });
  };

  return {
    auth,
    error,
    isLoading,
    user,
    authenticate,
    setIsLoading,
    setError,
    setUser,
    profile
  };
};

export default AuthLogic;
