import React from "react";
import "./App.css";
import ReactDOM from "react-dom/client";
import { ThemeProvider } from "styled-components";
import { theme } from "../src/infrastructure/theme";
import { BrowserRouter } from "react-router-dom";
import App from "./App";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </BrowserRouter>
  // </React.StrictMode>
);
