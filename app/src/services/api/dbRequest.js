export const mongoDBRequest = async (url, method, data, token) => {
  return fetch(`${process.env.REACT_APP_API}/${url}`, {
    method: `${method}`,
    credentials: "include",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      'Authorization': `Bearer ${token}`,
    },
    body: JSON.stringify(data),
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};




export const pythonRequest = async (url, method, data) => {

  return fetch(`http://localhost:8000/${url}`,{

    method: `${method}`,
    body: JSON.stringify(data),
    headers: {
      Accept: "application/json",
      'Content-type':'application/json', 
      
    },
  })
    .then((response) => {
      return response.json();
    })
    .catch((err) => console.log(err));
};
