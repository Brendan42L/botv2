import { React, useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import DashBoard from "./features/dashboard/dashboard.component";
import LogIn from "./features/log-in/log-in.component";
import Loading from "./features/loading/loading.component";
import { AuthContext } from "./utils/context";

import AuthLogic from "./utils/auth";

const App = () => {
  const { user, auth, isLoading, authenticate, setUser, profile } = AuthLogic();

  useEffect(() => {
    authenticate();
    setUser(profile);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AuthContext.Provider value={{ user: user }}>
      <Routes>
        {auth && !isLoading && <Route path="/" element={<DashBoard />} />}
        {!auth && !isLoading && <Route path="/" element={<LogIn />} />}
        {isLoading && <Route path="/" element={<Loading />} />}
      </Routes>
    </AuthContext.Provider>
  );
};

export default App;
