import React from "react";
import {
  Wrapper,
  LogInBtn,
  Wallpaper,
  InputContainer,
  InputContainerInner,
  UserName,
  Password,
  ErrorMessage,
} from "./log-in.styles";
import Logic from "./log-in.logic";

const LogIn = () => {
  const { handleLogIn, handleInputs, email, password, error } = Logic();
  return (
    <Wrapper>
      <Wallpaper />
      <InputContainer>
        <InputContainerInner>
          <UserName
          
            name="email"
            value={email}
            onChange={handleInputs}
            variant="standard"
            type="email"
            placeholder="Enter Email"
          />
          <Password
            name="password"
            onChange={handleInputs}
            value={password}
            variant="standard"
            type="password"
            placeholder="Enter Password"
          />

          <LogInBtn variant="outlined" onClick={handleLogIn}>
            Log In
          </LogInBtn>
          <ErrorMessage>{error}</ErrorMessage>
        </InputContainerInner>
      </InputContainer>
    </Wrapper>
  );
};

export default LogIn;
