import styled from "styled-components";
import { Button, FormControlLabel, TextField } from "@mui/material";

import { styled as Style } from "@mui/material/styles";

export const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`;

export const Wallpaper = styled.div`
  height: 100vh;
  width: 50%;
  background-image: url(./robot.jpeg);
  background-position: right;
  background-repeat: no-repeat;
  background-size: cover;
`;

export const InputContainer = styled.div`

  background-color: black;
  height: 100%;
  width: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const InputContainerInner = styled.div`
  border-radius: 1em;
  height: 40%;
  width: 60%;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: repeat(3, 1fr);
  grid-row-gap: 50px;
`;

export const ErrorMessage = styled.h5`
  color: red;
  font-weight: bold;
  text-align: center;
  font-size: ${(props) => props.theme.fontSizes.h5};
`;

export const FormControl = styled(FormControlLabel)``;

export const UserName = Style(TextField)({

  input: {
    '&:-webkit-autofill': {
      '-webkit-box-shadow': '0 0 0 100px black inset',
      '-webkit-text-fill-color': '#fff'
    },
    padding: "1em 0",
    color: "white",
    borderBottom: "1px solid white",
    "&::placeholder": {
      color: "white",
      
    },
  },
});

export const Password = Style(TextField)({
  input: {
    padding: "1em 0",
    color: "white",
    borderBottom: "1px solid white",
    "&::placeholder": {
      color: "white",
    },
  },
});

export const LogInBtn = Style(Button)({
  margin: "1em 0",
  color: "white",
  border: "1px solid white",
  width: `${(props) => props.theme.sizes[1]}`,
});
