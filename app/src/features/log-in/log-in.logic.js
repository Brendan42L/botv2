import { useState } from "react";
import { mongoDBRequest } from "../../services/api/dbRequest";
import { useNavigate } from "react-router-dom";
import AuthLogic from "../../utils/auth";

const Logic = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate();
  const { setIsLoading, error, setError } = AuthLogic();

  const handleLogIn = () => {
    setIsLoading(true);
    mongoDBRequest("sign-in", "POST", {
      email: email,
      password: password,
    })
      .then((data) => {
        if (data.token) {
          setIsLoading(false);
          localStorage.setItem("token", data.token);
          localStorage.setItem("user", JSON.stringify(data.user));
          navigate(0);
        } else {
          if (data === "User with that email does not exist") {
            setError(data);
          } else if (data === "Email and password do not match") {
            setError(data);
          } else {
            setError("Opps Something went wrong please check the console");
          }
        }
      })
      .catch((err) => {
        setIsLoading(false);
        console.log("Error", err);
      });
  };

  const handleInputs = (value) => {
    switch (value.target.name) {
      case "email":
        setEmail(value.target.value);
        break;
      case "password":
        setPassword(value.target.value);
        break;
      default:
        break;
    }
  };

  return {
    handleLogIn,
    handleInputs,
    email,
    password,
    error,
  };
};

export default Logic;
