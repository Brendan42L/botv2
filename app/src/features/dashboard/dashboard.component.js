import React from "react";
import { Wrapper, Robots, AllRobots } from "./dashboard.styles";
import Logic from "./dashboard.logic";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";

import SwipeableDrawer from "@mui/material/SwipeableDrawer";
import List from "@mui/material/List";
import Divider from "@mui/material/Divider";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import InboxIcon from "@mui/icons-material/MoveToInbox";
import MailIcon from "@mui/icons-material/Mail";

const DashBoard = () => {
  const { handleLogOut, user } = Logic();

  const [tab, setTab] = React.useState(0);

  const [state, setState] = React.useState({
    left: false,
  });

  const toggleDrawer = (anchor, open) => (event) => {
    if (
      event &&
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }

    setState({ ...state, [anchor]: open });
  };

  const list = (anchor) => (
    <Box
      sx={{ width: anchor === "top" || anchor === "bottom" ? "auto" : 250 }}
      role="presentation"
      onClick={toggleDrawer(anchor, false)}
      onKeyDown={toggleDrawer(anchor, false)}
    >
      <List>
        {["Dashboard", "Bot One", "Bot Two", "Bot Three", "Bot Four"].map(
          (text, index) => (
            <>
              <ListItem key={text} disablePadding>
                <ListItemButton onClick={() => handleScreenPick(index)}>
                  <ListItemIcon>
                    {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                  </ListItemIcon>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
              <Divider />
            </>
          )
        )}
      </List>
    </Box>
  );

  const PickScreen = () => {
    switch (tab) {
      case 0:
        return ""
      case 1:
        return <Robots src={"/robot-one.png"} alt="robot one" />;
      case 2:
        return <Robots src={"/robot-two.png"} alt="robot two" />;
      case 3:
        return <Robots src={"/robot-three.png"} alt="robot three" />;
      case 4:
        return <Robots src={"/robot-four.png"} alt="robot four" />;
      default:
        return "default";
    }
  };

  const handleScreenPick = (num) => {
    setTab(num);
  };

  return (
    <>
      <SwipeableDrawer
        anchor={"left"}
        open={state["left"]}
        onClose={toggleDrawer("left", false)}
        onOpen={toggleDrawer("left", true)}
      >
        {list("left")}
      </SwipeableDrawer>

      <AppBar
        position="static"
        sx={{
          backgroundImage:
            "linear-gradient(to top, #000000, #070707, #0e0e0e, #131313, #171717)",
          borderBottom: "1px solid #4F4F4F",
        }}
      >
        <Toolbar>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            sx={{ mr: 2 }}
            onClick={toggleDrawer("left", true)}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            color="primary"
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
          >
            Account: {user.name[0].toUpperCase() + user.name.substring(1)}
          </Typography>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, letterSpacing: "4px", marginRight: "10%" }}
          >
            <span style={{ color: "green", fontWeight: "bold" }}>+ 4.12% </span>
            Balance: $1740.20
          </Typography>

          <Button color="secondary" onClick={handleLogOut}>
            Log Out
          </Button>
        </Toolbar>
      </AppBar>
      <Wrapper maxWidth="xl">
        <PickScreen />
      </Wrapper>
    </>
  );
};

export default DashBoard;
