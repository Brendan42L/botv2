import { mongoDBRequest, pythonRequest } from "../../services/api/dbRequest";
import { useNavigate } from "react-router-dom";
import AuthLogic from "../../utils/auth";
import { useEffect } from "react";

const Logic = () => {
  const navigate = useNavigate();
  const { user } = AuthLogic();
  const token = localStorage.getItem("token");

  useEffect(() => {
    setInterval(() => {
      getBars();
    }, 30000);
  },[]);

  const handleLogOut = () => {
    mongoDBRequest("sign-out", "POST", { token: token }, token)
      .then((data) => {
        if (data === "goodbye") {
          localStorage.removeItem("token");
          localStorage.removeItem("user");
          navigate(0);
        }
      })
      .catch((err) => {
        console.log("Errror", err);
      });
  };

  const getBars = () => {
    mongoDBRequest("bars", "POST", { token: token })
      .then((bars) => {
        console.log("BARS SET");
        startBot(bars);
      })
      .catch((err) => {
        console.log("Errror", err);
      });
  };

  const startBot = async (data) => {
    pythonRequest("divergence", "POST", data)
      .then((signals) => {
        mongoDBRequest("calculation", "POST", {
          token: token,
          signals: signals,
        })
          .then((data) => {
            console.log("success", data);
          })
          .catch((err) => {
            console.log("Errror", err);
          });
      })
      .catch((err) => {
        console.log("Errror", err);
      });
  };

  return {
    handleLogOut,
    user,
    getBars,
    startBot,
  };
};

export default Logic;
