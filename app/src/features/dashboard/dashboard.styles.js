import styled from "styled-components";
import { Container } from "@mui/material";

import { styled as Style } from "@mui/material/styles";

export const Wrapper = Style(Container)({
  backgroundImage:
    "linear-gradient(to right top, #07041f, #0f0017, #0f000e, #0b0005, #000000)",
  margin: "0",
  padding: "0",
  height: "100vh",
  overflowY: "hidden",
  color: "white",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
});

export const Robots = styled.img({
  objectFit: "cover",
  width: "250px",
  height: "350px",
  borderRadius: "20px",
});

export const AllRobots = styled.div({
  margin: "25% auto 0 auto",
  display: "grid",
  gridTemplateColumns: "repeat(4, 1fr)",
  gridTemplateRows: "repeat(2, 1fr)",
  gridColumnGap: "50px",
  gridRowGap: "50px",
});
