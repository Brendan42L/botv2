import React from "react";

const Loading = () => {
  return <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}><h1>Loading....</h1></div>;
};

export default Loading;
