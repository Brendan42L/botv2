export const colors = {
  brand: {
    primary: "black",
    secondary: "#",
    muted: "#",
  },
  ui: {
    primary: "blue",
    secondary: "#",
    tertiary: "#",
    quaternary: "#",
    disabled: "#",
    error: "#",
    success: "#",
  },
  bg: {
    primary: "#",
    secondary: "#",
  },
  text: {
    primary: "#",
    secondary: "#",
    disabled: "#",
    inverse: "#",
    error: "#",
    success: "#",
  },
};
